"""
Time stepping schemes to solve ROM model.
"""
import numpy as np
from parameters import *


# Finite differences schemes to solve ROM
def forward(setup, initial_h, initial_T, alpha, r, gamma, nt, dt, mu=2 / 3):
    """
    Forward in time finite differences scheme to solve for h (depth) and T
    (SST anomaly) for nt times steps and parameters from a setup dictionary
    """
    # Derived parameters
    b, R = derived_paras(setup, mu)

    # new time-step arrays for h and T
    h = np.zeros(nt + 1)
    T = np.zeros(nt + 1)

    # initial conditions
    h[0] = initial_h
    T[0] = initial_T

    for i in range(nt):
        h[i + 1] = h[i] + dt * (-r * h[i] - alpha * b * T[i])
        T[i + 1] = T[i] + dt * (R * T[i] + gamma * h[i])

    return h, T


def centred(setup, initial_h, initial_T, alpha, r, gamma, nt, dt, mu=2 / 3):
    """
    Centred in time (Leapfrog) finite differences scheme to solve for h (thermocline depth) and T
    (SST anomaly) for nt times steps and parameters from a setup dictionary. First time step uses
    forward in time numerical scheme.
    """
    b, R = derived_paras(setup, mu)

    h, T = np.zeros(nt + 1), np.zeros(nt + 1)

    h[0], T[0] = initial_h, initial_T

    # first time step uses forward in time function
    h[1] = h[0] + dt * (-r * h[0] - alpha * b * T[0])
    T[1] = T[0] + dt * (R * T[0] + gamma * h[0])

    for i in range(1, nt):
        h[i + 1] = h[i - 1] + 2 * dt * (-r * h[i] - alpha * b * T[i])
        T[i + 1] = T[i - 1] + 2 * dt * (R * T[i] + gamma * h[i])

    return h, T


# function for ROM
def f(h, T, setup, mu, b, R, e_n=0, xi_1=0):
    """
    ROM function contains the ODEs to be solved for h and T, setup is the dictionary
    of parameters and additional parameters mu, b, R, e_n, sigma_1
    """
    # set parameters
    bo = setup["bo"]
    gamma = setup["gamma"]
    c = setup["c"]
    r = setup["r"]
    alpha = setup["alpha"]
    xi_2 = setup["xi_2"]

    # Initialize the ROM equations
    dhdt = -r * h - alpha * b * T - alpha * xi_1
    dTdt = R * T + gamma * h - e_n * (h + b * T) ** 3 + gamma * xi_1 + xi_2
    return dhdt, dTdt


def rk4(initial_h, initial_T, setup, nt, dt, mu=2 / 3, e_n=0):
    """
    Iterative 4th order Runge_Kutta scheme solves ROM for initial conditions,
    setup parameters, nt time steps and dt is the step size.
    Mu (couping coefficient) is set to the critical value 2/3 and
    e_n (non-linearity) is set to 0 unless changed.
    """

    h, T = np.zeros(nt + 1), np.zeros(nt + 1)

    h[0], T[0] = initial_h, initial_T

    b, R = derived_paras(setup, mu)

    # RK4 routine
    k1, k2, k3, k4 = np.zeros(2), np.zeros(2), np.zeros(2), np.zeros(2)

    for i in range(nt):
        k1 = f(h[i], T[i], setup, mu, b, R, e_n)
        k2 = f(h[i] + k1[0] * dt / 2, T[i] + k1[1] * dt / 2, setup, mu, b, R, e_n)
        k3 = f(h[i] + k2[0] * dt / 2, T[i] + k2[1] * dt / 2, setup, mu, b, R, e_n)
        k4 = f(h[i] + k3[0] * dt, T[i] + k3[1] * dt, setup, mu, b, R, e_n)
        h[i + 1] = h[i] + dt * (k1[0] + 2 * k2[0] + 2 * k3[0] + k4[0]) / 6
        T[i + 1] = T[i] + dt * (k1[1] + 2 * k2[1] + 2 * k3[1] + k4[1]) / 6
    return h, T


# Amended runge-kutta scheme for annual cycle variation
def rk4_annual(initial_h, initial_T, setup, tau, nt, dt, e_n=0.1):
    """
    Iterative 4th order Runge_Kutta scheme with coupling parameter to vary on an annual cycle,
    solves ROM for initial conditions, setup parameters, tau from setup, nt time steps and
    dt is the step size, e_n (non-linearity) is set to 0.1 unless changed.
    """
    # set parameters for changing derived parameters
    bo = setup["bo"]
    gamma = setup["gamma"]
    c = setup["c"]
    mu0 = setup["mu0"]
    muann = setup["muann"]

    h, T = np.zeros(nt + 1), np.zeros(nt + 1)

    t = np.arange(0, (nt +1) * dt, dt)

    h[0], T[0] = initial_h, initial_T

    k1, k2, k3, k4 = np.zeros(2), np.zeros(2), np.zeros(2), np.zeros(2)
    mu, b, R = 0, 0, 0

    for i in range(nt):
        # calculate new coupling parameter (mu) for the annual cycle
        mu = mu0 * (1 + muann * np.cos(2 * np.pi * t[i] / tau - 5 * np.pi / 6))
        b = bo * mu
        R = gamma * b - c

        k1 = f(h[i], T[i], setup, mu, b, R, e_n)
        k2 = f(h[i] + k1[0] * dt / 2, T[i] + k1[1] * dt / 2, setup, mu, b, R, e_n)
        k3 = f(h[i] + k2[0] * dt / 2, T[i] + k2[1] * dt / 2, setup, mu, b, R, e_n)
        k4 = f(h[i] + k3[0] * dt, T[i] + k3[1] * dt, setup, mu, b, R, e_n)
        h[i + 1] = h[i] + dt * (k1[0] + 2 * k2[0] + 2 * k3[0] + k4[0]) / 6
        T[i + 1] = T[i] + dt * (k1[1] + 2 * k2[1] + 2 * k3[1] + k4[1]) / 6
    return h, T


# Amended runge-kutta scheme for additional wind stress forcing
def rk4_wind(initial_h, initial_T, setup, tau, tau_cor, nt, dt, e_n=0, w=None):
    """
   Iterative 4th order Runge_Kutta scheme with wind stress forcing,
    solves ROM for initial conditions, setup parameters, tau and tau_cor from setup,
    nt time steps and dt is the step size, e_n (non-linearity) is set to 0 unless changed.
    W, white noise is random unless given otherwise.
    """
    bo, gamma, c, mu0, muann, fann, fran = setup["bo"], setup["gamma"], setup["c"], setup["mu0"], setup["muann"], \
                                           setup["fann"], setup["fran"]
    h, T = np.zeros(nt + 1), np.zeros(nt + 1)

    t = np.arange(0, (nt + 1) * dt, dt)

    h[0], T[0] = initial_h, initial_T

    k1, k2, k3, k4 = np.zeros(2), np.zeros(2), np.zeros(2), np.zeros(2)
    mu, b, R, xi_1, W = 0, 0, 0, 0, 0

    for i in range(nt):
        mu = mu0 * (1 + muann * np.cos(2 * np.pi * t[i] / tau - 5 * np.pi / 6))
        b = bo * mu
        R = gamma * b - c

        if w is None:
            # White noise different after every tau_cor (=dt)
            W = np.random.uniform(-1, 1)

        else:  # in Task G we will need to remove the random wind forcing
            W = w

        # Add wind stress forcing
        xi_1 = fann * np.cos(2 * np.pi * t[i] / tau) + fran * W * tau_cor / dt

        k1 = f(h[i], T[i], setup, mu, b, R, e_n, xi_1)
        k2 = f(h[i] + k1[0] * dt / 2, T[i] + k1[1] * dt / 2, setup, mu, b, R, e_n, xi_1)
        k3 = f(h[i] + k2[0] * dt / 2, T[i] + k2[1] * dt / 2, setup, mu, b, R, e_n, xi_1)
        k4 = f(h[i] + k3[0] * dt, T[i] + k3[1] * dt, setup, mu, b, R, e_n, xi_1)
        h[i + 1] = h[i] + dt * (k1[0] + 2 * k2[0] + 2 * k3[0] + k4[0]) / 6
        T[i + 1] = T[i] + dt * (k1[1] + 2 * k2[1] + 2 * k3[1] + k4[1]) / 6

    return h, T
