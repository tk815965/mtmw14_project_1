"""
File to run Tasks A-G for solving ROM
"""
from schemes import *
from parameters import *
import matplotlib.pyplot as plt
import numpy as np


def task_A(setup, nt, dt):
    """
    Three finite difference schemes to solve ROM, parameters from the setup,
    nt time steps and dt is step length. Produce time series plots for T and h,
    and a phase plot for each scheme.
    """

    # set time array
    t = np.arange(0, (nt + 1) * dt, dt)

    # Non-dimensionalised temperature, depth and time
    h_nd = nondimensionalise_h(setup["initial_h"])
    T_nd = nondimensionalise_T(setup["initial_T"])
    dt_nd = nondimensionalise_t(dt)  # non-dimensionalise dt

    # solve using forward in time differences scheme
    result = forward(setup, h_nd, T_nd, setup["alpha"], setup["r"], setup["gamma"], nt, dt_nd)
    # solve using centred in time differences scheme
    result1 = centred(setup, h_nd, T_nd, setup["alpha"], setup["r"],
                      setup["gamma"], nt, dt_nd)
    # solve using RK4
    result2 = rk4(h_nd, T_nd, setup, nt, dt_nd)

    # redomensionalise temperature and depth
    h_forward = redimensionalise_h(result[0])
    T_forward = redimensionalise_T(result[1])
    h_centred = redimensionalise_h(result1[0])
    T_centred = redimensionalise_T(result1[1])
    h_rk = redimensionalise_h(result2[0])
    T_rk = redimensionalise_T(result2[1])

    # Plot SST time series
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3, sharey=True, figsize=(20, 4.8))
    fig.suptitle('SST Anomaly Time Series')
    ax1.plot(t, T_forward)
    ax1.set_title('Forward in Time')
    ax2.plot(t, T_centred)
    ax2.set_title('Centred in Time')
    ax3.plot(t, T_rk)
    ax3.set_title('Runge-Kutta')
    ax1.set_xlabel('Time (months)')
    ax2.set_xlabel('Time (months)')
    ax3.set_xlabel('Time (months)')
    ax1.set_ylabel('SST Anomaly (K)')
    plt.show()
    print('Figure 1: Time series plots for SST anomaly over one period for the three numerical schemes.')

    # Plot depth time series
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3, sharey=True, figsize=(20, 4.8))
    fig.suptitle('Thermocline Depth Time Series')
    ax1.plot(t, h_forward)
    ax1.set_title('Forward in Time')
    ax2.plot(t, h_centred)
    ax2.set_title('Centred in Time')
    ax3.plot(t, h_rk)
    ax3.set_title('Runge-Kutta')
    ax1.set_xlabel('Time (months)')
    ax2.set_xlabel('Time (months)')
    ax3.set_xlabel('Time (months)')
    ax1.set_ylabel('Thermocline Depth (m)')
    plt.show()
    print('Figure 2: Time series plots for thermocline depth over one period for the three numerical schemes.')

    # Plot phase plots for foward in time
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3, sharey=True, figsize=(20, 4.8))
    fig.suptitle('Phase Plots')
    ax1.plot(h_forward, T_forward)
    ax1.set_title('Forward in Time')
    ax2.plot(h_centred, T_centred)
    ax2.set_title('Centred in Time')
    ax3.plot(h_rk, T_rk)
    ax3.set_title('Runge-Kutta')
    ax1.set_xlabel('Thermocline Depth (m)')
    ax2.set_xlabel('Thermocline Depth (m)')
    ax3.set_xlabel('Thermocline Depth (m)')
    ax1.set_ylabel('SST Anomaly (K)')
    plt.show()
    print('Figure 3: Phase plots for thermocline depth and SST anomaly over one period for the three numerical schemes.')


def task_A_stability(setup, nt, dt):
    """
    Test to stability of the finite differences schemes by running for multiple
    periods, nt.  Parameters from the setup and dt is step length.
    """
    t = np.arange(0, (nt + 1) * dt, dt)

    h_nd, T_nd, dt_nd = nondimensionalise_h(setup["initial_h"]), \
                        nondimensionalise_T(setup["initial_T"]), \
                        nondimensionalise_t(dt)
    result, result1, result2 = forward(setup, h_nd, T_nd, setup["alpha"], setup["r"], setup["gamma"], nt, dt_nd), \
                               centred(setup, h_nd, T_nd, setup["alpha"], setup["r"], setup["gamma"], nt, dt_nd), \
                               rk4(h_nd, T_nd, setup, nt, dt_nd)

    h_forward, T_forward = redimensionalise_h(result[0]), redimensionalise_T(result[1])
    h_centred, T_centred = redimensionalise_h(result1[0]), redimensionalise_T(result1[1])
    h_rk, T_rk = redimensionalise_h(result2[0]), redimensionalise_T(result2[1])

    # Plot phase plots for foward in time
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(20, 4.8))
    fig.suptitle(f'Phase Plots for {nt} time steps')
    ax1.plot(h_forward, T_forward)
    ax1.set_title('Forward in Time')
    ax2.plot(h_centred, T_centred)
    ax2.set_title('Centred in Time')
    ax3.plot(h_rk, T_rk)
    ax3.set_title('Runge-Kutta')
    ax1.set_xlabel('Thermocline Depth (m)')
    ax2.set_xlabel('Thermocline Depth (m)')
    ax3.set_xlabel('Thermocline Depth (m)')
    ax1.set_ylabel('SST Anomaly (K)')
    plt.show()
    print(f'Figure 4: Phase plots for thermocline depth and SST anomaly for 30,000 time steps period for the three '
          f'numerical schemes to test the stability of the schemes.')


def task_B(setup, nt, dt):
    """
    Solve using Rk4 for mu = 0.75 and 0.5. Parameters from the setup,
    nt time steps and dt is step length. Produce time series plots for T and h,
    and a phase plot for each value of mu.
    """
    t = np.arange(0, (nt + 1) * dt, dt)

    h_nd, T_nd, dt_nd = nondimensionalise_h(setup["initial_h"]), \
                        nondimensionalise_T(setup["initial_T"]), \
                        nondimensionalise_t(dt)

    big_mu = rk4(h_nd, T_nd, setup, nt, dt_nd, mu=0.75)
    small_mu = rk4(h_nd, T_nd, setup, nt, dt_nd, mu=0.5)

    h_big = redimensionalise_h(big_mu[0])
    T_big = redimensionalise_T(big_mu[1])
    h_small = redimensionalise_h(small_mu[0])
    T_small = redimensionalise_T(small_mu[1])

    # Plot graphs for small mu
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(20, 4.8))
    fig.suptitle('Sub-critical coupling parameter')
    ax1.plot(t, T_small)
    ax1.set_title('SST Anomaly time series')
    ax1.set_xlabel('Time (months)')
    ax1.set_ylabel('SST Anomaly (K)')
    ax2.plot(t, h_small)
    ax2.set_title('Depth time series')
    ax2.set_xlabel('Time (months)')
    ax2.set_ylabel('Thermocline Depth (m)')
    ax3.plot(h_small, T_small)
    ax3.set_title('Phase plot')
    ax3.set_xlabel('Thermocline Depth (m)')
    ax3.set_ylabel('SST Anomaly (K)')
    plt.show()
    print('Figure 5: Time series and phase plots over 5 periods for SST anomaly and thermocline depth with '
          'a subcritical coupling parameter.')

    # Plot graphs for large mu
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(20, 4.8))
    fig.suptitle('Super-critical coupling parameter')
    ax1.plot(t, T_big)
    ax1.set_title('SST Anomaly time series')
    ax1.set_xlabel('Time (months)')
    ax1.set_ylabel('SST Anomaly (K)')
    ax2.plot(t, h_big)
    ax2.set_title('Depth time series')
    ax2.set_xlabel('Time (months)')
    ax2.set_ylabel('Thermocline Depth (m)')
    ax3.plot(h_big, T_big)
    ax3.set_title('Phase plot')
    ax3.set_xlabel('Thermocline Depth (m)')
    ax3.set_ylabel('SST Anomaly (K)')
    plt.show()
    print('Figure 6: Time series and phase plots over 5 periods for SST anomaly and thermocline depth with '
          'a subcritical coupling parameter.')


def task_C(setup, nt, dt):
    """
    Solve ROM using RK4 with non-linearity (e_n) turned on, for
    mu=0.5, 0.75. Parameters from the setup, nt time steps and dt is step length.
    """
    # change to non-linearity
    setup["e_n"] = 0.1

    t = np.arange(0, (nt + 1) * dt, dt)

    h_nd, T_nd, dt_nd = nondimensionalise_h(setup["initial_h"]), \
                        nondimensionalise_T(setup["initial_T"]), \
                        nondimensionalise_t(dt)

    # Non-linearity with critical coupling coefficient
    result = rk4(h_nd, T_nd, setup, nt, dt_nd, e_n=0.1)

    # Non-linearity with super-critical coupling coefficient
    result1 = rk4(h_nd, T_nd, setup, nt, dt_nd, 0.75, 0.1)

    h_rk = redimensionalise_h(result[0])
    T_rk = redimensionalise_T(result[1])
    h_1 = redimensionalise_h(result1[0])
    T_1 = redimensionalise_T(result1[1])

    # Phase plots for non-linearity
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(15, 4.8))
    fig.suptitle('Non-Linearity')
    ax1.plot(h_rk, T_rk)
    ax1.plot(h_rk[-1], T_rk[-1], 'k*')
    ax1.set_title('Critical coupling')
    ax1.set_xlabel('Thermocline Depth (m)')
    ax1.set_ylabel('SST Anomaly (K)')
    ax2.plot(h_1, T_1)
    ax2.plot(h_1[-1], T_1[-1], 'k*')
    ax2.set_title('Super-critical coupling')
    ax2.set_xlabel('Thermocline Depth (m)')
    ax2.set_ylabel('SST Anomaly (K)')
    plt.show()
    print('Figure 7: Phase plots over 5 periods for SST anomaly and thermocline depth with the introduction of '
          'non-linearity. Run for critical and subcritical coupling parameter.')


def task_D(setup, nt, dt):
    """
    Modified ROM to include the annual cycle in coupling parameter.
    Parameters from the setup, nt time steps and dt is step length.
    """
    t = np.arange(0, (nt+1) * dt, dt)

    h_nd, T_nd, dt_nd, tau_nd = nondimensionalise_h(setup["initial_h"]), \
                                nondimensionalise_T(setup["initial_T"]),\
                                nondimensionalise_t(dt), \
                                nondimensionalise_t(setup["tau"])

    result = rk4_annual(h_nd, T_nd, setup, tau_nd, nt, dt_nd)

    h = redimensionalise_h(result[0])
    T = redimensionalise_T(result[1])

    # Plot graphs annual cycle
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize = (20, 4.8))
    fig.suptitle('Annual Cycle')
    ax1.plot(t, T)
    ax1.set_title('SST Anomaly time series')
    ax1.set_xlabel('Time (months)')
    ax1.set_ylabel('SST Anomaly (K)')
    ax2.plot(t, h)
    ax2.set_title('Depth time series')
    ax2.set_xlabel('Time (months)')
    ax2.set_ylabel('Thermocline Depth (m)')
    ax3.plot(h, T)
    ax3.set_title('Phase plot')
    ax3.set_xlabel('Thermocline Depth (m)')
    ax3.set_ylabel('SST Anomaly (K)')
    plt.show()
    print('Figure 8: Time series and phase plots over 5 periods for SST anomaly and thermocline depth with '
          'a the addition of the annual cycle.')


def task_EF(setup, nt, dt):
    """
    Modified ROM to include the wind forcing and vary linearity.
    Parameters from the setup, nt time steps and dt is step length.
    """
    t = np.arange(0, (nt + 1) * dt, dt)
    h_nd, T_nd, dt_nd, tau_nd, tau_cor_nd = nondimensionalise_h(setup["initial_h"]), \
                                            nondimensionalise_T(setup["initial_T"]), \
                                            nondimensionalise_t(dt), \
                                            nondimensionalise_t(setup["tau"]), \
                                            nondimensionalise_t(setup["tau_cor"])

    result = rk4_wind(h_nd, T_nd, setup, tau_nd, tau_cor_nd, nt, dt_nd)

    # Change e_n to turn on non-linearity
    result1 = rk4_wind(h_nd, T_nd, setup, tau_nd, tau_cor_nd, nt, dt_nd, 0.1)

    h = redimensionalise_h(result[0])
    T = redimensionalise_T(result[1])
    h_e = redimensionalise_h(result1[0])
    T_e = redimensionalise_T(result1[1])

    # Phase plots for non-linearity
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(15, 4.8))
    fig.suptitle('Stochastic Wind Forcing on the linear model')
    ax1.plot(t, T)
    ax1.set_title('SST Anomaly time series')
    ax1.set_xlabel('Time (months)')
    ax1.set_ylabel('SST Anomaly (K)')
    ax2.plot(h, T)
    ax2.plot(h[-1], T[-1], 'k*')
    ax2.set_title('Phase plot')
    ax2.set_xlabel('Thermocline Depth (m)')
    ax2.set_ylabel('SST Anomaly (K)')
    plt.show()
    print('Figure 9: SST time series and phase plot over 5 periods for the linear model, with the addition of '
          'stochastic wind forcing.')

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(15, 4.8))
    fig.suptitle('Stochastic Wind Forcing on the non-linear model')
    ax1.plot(t, T_e)
    ax1.set_title('SST Anomaly time series')
    ax1.set_xlabel('Time (months)')
    ax1.set_ylabel('SST Anomaly (K)')
    ax2.plot(h_e, T_e)
    ax2.plot(h_e[-1], T_e[-1], 'k*')
    ax2.set_title('Phase plot')
    ax2.set_xlabel('Thermocline Depth (m)')
    ax2.set_ylabel('SST Anomaly (K)')
    plt.show()
    print('Figure 10: SST time series and phase plot over 5 periods for the non-linear model, with the addition of '
          'stochastic wind forcing.')


def task_G(setup, nt, dt, n):
    """
    Ensemble forecast from modified ROM to include the wind forcing and vary linearity.
    Parameters from the setup, nt time steps and dt is step length, n is number of forecasts.
    """

    t = np.arange(0, (nt + 1) * dt, dt)
    dt_nd, tau_nd, tau_cor_nd = nondimensionalise_t(dt), nondimensionalise_t(setup["tau"]), \
                                nondimensionalise_t(setup["tau_cor"])
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(20, 4.8))
    fig.suptitle('Ensemble Phase Planes')
    # Ensemble for random initial condition and random wind forcing
    for i in range(n):
        setup_new = setup.copy()
        # Generate initial condition uncertainty
        h_nd = nondimensionalise_h(setup_new["initial_h"] + np.random.normal(0, 0.1))
        T_nd = nondimensionalise_T(setup_new["initial_T"] + np.random.normal(0, 0.1))

        result = rk4_wind(h_nd, T_nd, setup, tau_nd, tau_cor_nd, nt, dt_nd, 0.1)

        # Add new forecast to the phase plot
        ax1.plot(redimensionalise_h(result[0]), redimensionalise_T(result[1]), 'b', zorder=1)
        # Add initial points to plot
        ax1.plot(redimensionalise_h(h_nd), redimensionalise_T(T_nd), 'rx', zorder=2)
        # Add end points to plot
        ax1.plot(redimensionalise_h(result[0][-1]), redimensionalise_T(result[1][-1]), 'k*', zorder=3)

    ax1.set_xlabel('Thermocline Depth (m)')
    ax1.set_ylabel('SST Anomaly (K)')
    ax1.set_title('Random initial conditions and wind forcing')

    # Ensemble for only random wind forcing
    for j in range(n):
        result = rk4_wind(nondimensionalise_h(setup["initial_h"]),
                          nondimensionalise_T(setup["initial_T"]), setup,
                          tau_nd, tau_cor_nd, nt, dt_nd, 0.1)

        # Add new forecast to the phase plot
        ax2.plot(redimensionalise_h(result[0]), redimensionalise_T(result[1]), 'g', zorder=1)
        ax2.plot(redimensionalise_h(result[0][-1]), redimensionalise_T(result[1][-1]), 'k*', zorder=2)

    ax2.set_xlabel('Thermocline Depth (m)')
    ax2.set_ylabel('SST Anomaly (K)')
    ax2.set_title('Random wind forcing')

    # Ensemble for random initial condition and random wind forcing
    for k in range(n):
        setup_new = setup.copy()
        h_nd = nondimensionalise_h(setup_new["initial_h"] + np.random.normal(0, 0.1))
        T_nd = nondimensionalise_T(setup_new["initial_T"] + np.random.normal(0, 0.1))

        result = rk4_wind(h_nd, T_nd, setup, tau_nd, tau_cor_nd, nt, dt_nd, 0.1, 0)

        # Add new forecast to the phase plot
        ax3.plot(redimensionalise_h(result[0]), redimensionalise_T(result[1]), 'c', zorder=1)
        ax3.plot(redimensionalise_h(h_nd), redimensionalise_T(T_nd), 'rx', zorder=2)
        ax3.plot(redimensionalise_h(result[0][-1]), redimensionalise_T(result[1][-1]), 'k*', zorder=3)

    ax3.set_xlabel('Thermocline Depth (m)')
    ax3.set_ylabel('SST Anomaly (K)')
    ax3.set_title('Random initial conditions')
    plt.show()

    print('Figure 11: Phase planes for 20 ensembles forecasts over 5 periods')

    fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(20, 4.8))
    fig.suptitle('Ensemble Plume Diagrams')
    # Ensemble for random initial condition and random wind forcing
    for i in range(n):
        setup_new = setup.copy()

        h_nd = nondimensionalise_h(setup_new["initial_h"] + np.random.normal(0, 0.1))
        T_nd = nondimensionalise_T(setup_new["initial_T"] + np.random.normal(0, 0.1))

        result = rk4_wind(h_nd, T_nd, setup, tau_nd, tau_cor_nd, nt, dt_nd, 0.1)

        ax1.plot(t, redimensionalise_T(result[1]), 'b', zorder=1)
        ax1.plot(0, redimensionalise_T(T_nd), 'rx', zorder=2)
        ax1.plot((nt + 1) * dt, redimensionalise_T(result[1][-1]), 'k*', zorder=3)
    ax1.set_title('random initial conditions and wind forcing')
    ax1.set_xlabel('Time (months)')
    ax1.set_ylabel('SST Anomaly (K)')
    # Ensemble for only random wind forcing
    for j in range(n):
        result = rk4_wind(nondimensionalise_h(setup["initial_h"]),
                          nondimensionalise_T(setup["initial_T"]), setup,
                          tau_nd, tau_cor_nd, nt, dt_nd, 0.1)

        ax2.plot(t, redimensionalise_T(result[1]), 'g', zorder=1)
        ax2.plot((nt + 1) * dt, redimensionalise_T(result[1][-1]), 'k*', zorder=2)

    ax2.set_xlabel('Time (months)')
    ax2.set_ylabel('SST Anomaly (K)')
    ax2.set_title('Random wind forcing')
    # Ensemble for random initial condition and random wind forcing
    for k in range(n):
        setup_new = setup.copy()
        h_nd = nondimensionalise_h(setup_new["initial_h"] + np.random.normal(0, 0.1))
        T_nd = nondimensionalise_T(setup_new["initial_T"] + np.random.normal(0, 0.1))
        result = rk4_wind(h_nd, T_nd, setup, tau_nd, tau_cor_nd, nt, dt_nd, 0.1, 0)

        ax3.plot(t, redimensionalise_T(result[1]), 'c', zorder=1)
        ax3.plot(0, redimensionalise_T(T_nd), 'rx', zorder=2)
        ax3.plot((nt + 1) * dt, redimensionalise_T(result[1][-1]), 'k*', zorder=3)
    ax3.set_title('Random initial conditions')
    ax3.set_xlabel('Time (months)')
    ax3.set_ylabel('SST Anomaly (K)')
    plt.show()
    print('Figure 12: SST time series for 20 ensembles forecasts over 5 periods.')

if __name__ == "__main__":
    task_A(setup1.copy(), 41, 1)
    task_A_stability(setup1.copy(), 30000, 1)
    task_B(setup1.copy(), 205, 1)
    task_C(setup1.copy(), 205, 1)
    task_D(annual_wind, 205, 1)
    task_EF(annual_wind, 6150, 1/30)
    task_G(annual_wind, 6150, 1 / 30, 20)
