# MTMW14_Project_1

**parameters.py**
holds dictionary of parameters and dimensionalising functions

**schemes.py**
holds numerical schemes to solve ODEs

**main.py**
runs model and solves using schemes in schemes.py


**HOW TO RUN**: download parameters.py and schemes.py and then run main.py

-----------------------------------------------------------------------------


MTMW14_Project_1 is the Jupyter notebook file for the project
