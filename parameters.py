"""
Dictionary's of parameters for ROM and dimensionalising functions
"""

# Dictionary of the parameters used in Task A - C

setup1 = {'initial_h': 0,       # Initial Thermocline Depth
          'initial_T': 1.125,   # Intitial SST anomaly
          'bo': 2.5,            # high-end value of the coupling parameter
          'gamma': 0.75,        # feedback of thermocline gradient
          'c': 1,               # damping rate
          'r': 0.25,            # damping of upper ocean heat content
          'alpha': 0.125,       # relates wind stress to recharge
          'e_n' : 0,            # degree of non-linearity
          'xi_1' : 0,           # random wind stress forcing
          'xi_2' : 0            # random heating
}

# Dictionary of the parameters used in Task D - G
annual_wind = {'initial_h': 0,
          'initial_T': 1.125,
          'bo': 2.5,
          'gamma': 0.75,
          'c' : 1,
          'r': 0.25,
          'alpha': 0.125,
          'xi_2' : 0,
          'mu0': 0.75,
          'muann': 0.2,
          'fann' : 0.02,
          'fran' : 0.2,
          'tau' : 12,
          'tau_cor': 1/30           #assumed there are 30 days in a month
}

def derived_paras(setup, mu):
    """
    Derive the parameters b (thermocline slope) and R (Bjerknes positive feedback process)
    from the setup used and value of mu (coupling coefficient)
    """
    b = setup["bo"]*mu
    R = setup["gamma"]*b -setup["c"]
    return b, R

#Functions to non-dimensionalise and re-dimensionalise temperature, depth and time

def nondimensionalise_h(h):
    """
    Non-dimensionalise the thermocline depth by dividing by 150m
    """
    h_New = h/150
    return h_New

def nondimensionalise_T(T):
    """
    Non-dimensionalise the SST anomaly (temperature) by dividing by 7.5K
    """
    T_New = T/7.5
    return T_New

def nondimensionalise_t(t):
    """
    Non-dimensionalise time by dividing by 41 months
    """
    t_New = t/2
    return t_New

def redimensionalise_h(h):
    """
    Re-dimensionalise the thermocline depth by multiplying by 150m
    """
    h_New = h*150
    return h_New

def redimensionalise_T(T):
    """
    Re-dimensionalise the SST anomaly (temperature) by multiplying by 7.5K
    """
    T_New = T*7.5
    return T_New

def redimensionalise_t(t):
    """
    Re-dimensionalise time by multiplying by 41 months
    """
    t_New = t*2
    return t_New
